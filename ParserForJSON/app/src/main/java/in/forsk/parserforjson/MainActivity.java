package in.forsk.parserforjson;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import in.forsk.parserforjson.wrapper.FacultyWrapper;

public class MainActivity extends AppCompatActivity {

    TextView tv;
    Button btnDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.textView1);
        btnDownload = (Button) findViewById(R.id.button1);

        btnDownload.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //Write the code to read the JSON file from raw folder.

                try {
                    String raw_JSON = Utils.getStringFromRaw(MainActivity.this, R.raw.faculty_profile_code);
//                    tv.setText(raw_JSON);
                    showFormattedJSON  (raw_JSON);
                } catch (IOException e) {
                    e.printStackTrace();
                    tv.setText(e.getMessage());
                }
            }
        });
    }

    public void showParsedJSON(String raw_JSON) {

        try {

            JSONArray facultyArray = new JSONArray(raw_JSON);

            StringBuilder sb = new StringBuilder();

            for (int i = 0; facultyArray.length() > i; i++) {

                JSONObject facultyObj = facultyArray.getJSONObject(i);

                sb.append("##############"+(i+1)+"###############");
                sb.append(System.getProperty("line.separator"));

                sb.append("first_name ->");
                sb.append(System.getProperty("line.separator"));


                sb.append("    "+facultyObj.getString("first_name"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));

                sb.append("last_name ->");
                sb.append(System.getProperty("line.separator"));

                sb.append("    "+facultyObj.getString("last_name"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));

                sb.append("photo ->");
                sb.append(System.getProperty("line.separator"));

                sb.append("    "+facultyObj.getString("photo"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));

                sb.append("department ->");
                sb.append(System.getProperty("line.separator"));

                sb.append("    "+facultyObj.getString("department"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));

                sb.append("reserch_area ->");
                sb.append(System.getProperty("line.separator"));

                sb.append("    "+facultyObj.getString("reserch_area"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));

                JSONObject interest_areas = facultyObj.getJSONObject("interest_areas");

                sb.append("interest_areas ->");
                sb.append(System.getProperty("line.separator"));

                sb.append("        "+interest_areas.getString("subject_1"));
                sb.append(System.getProperty("line.separator"));

                sb.append("        "+interest_areas.getString("subject_2"));
                sb.append(System.getProperty("line.separator"));

                sb.append("        "+interest_areas.getString("subject_3"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));


                JSONObject contact_details = facultyObj.getJSONObject("contact_details");

                sb.append("contact_details ->");
                sb.append(System.getProperty("line.separator"));


                sb.append("        "+contact_details.getString("phone"));
                sb.append(System.getProperty("line.separator"));

                sb.append("        "+contact_details.getString("email"));
                sb.append(System.getProperty("line.separator"));
                sb.append(System.getProperty("line.separator"));

            }

            sb.append("#############################");

            tv.setText(sb);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void showFormattedJSON(String raw_JSON) {

        ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_JSON);

        StringBuilder sb = new StringBuilder();

        for (FacultyWrapper facultyWrapper : facultyWrapperArrayList) {

            sb.append("#############################");
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getFirst_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getLast_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getPhoto());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getDepartment());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getReserch_area());
            sb.append(System.getProperty("line.separator"));

            ArrayList<String> interest_areas = facultyWrapper.getInterest_areas();

            for (String s : interest_areas) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }

            ArrayList<String> contact_details = facultyWrapper.getContact_details();

            for (String s : interest_areas) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }

        }

        sb.append("#############################");

        tv.setText(sb);


    }

}
